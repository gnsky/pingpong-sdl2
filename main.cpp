#include "SDL2/SDL.h"
#include <SDL2/SDL_image.h>
#include <stdio.h>

using namespace std;

const int WINDOW_WIDTH = 640;
const int WINDOW_HEIGHT = 480;

const int SDL_INIT_ERROR_CODE = -1;

int main(int argc, char* args[])
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("DEBUG: SDL initialization error: %s\n", SDL_GetError());
        return SDL_INIT_ERROR_CODE;
    }

    SDL_Window* window = SDL_CreateWindow("PingPong!",
                                          SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                                          WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_SHOWN);

    if (window == NULL) {
        SDL_LogError(1, "SDL window initialization error: %s\n", SDL_GetError());
        return SDL_INIT_ERROR_CODE;
    }

    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == NULL) {
        SDL_LogError(1, "SDL renderer initialization error: %s\n", SDL_GetError());
        return SDL_INIT_ERROR_CODE;
    }

    SDL_Texture* playerTexture = IMG_LoadTexture(renderer, "./assets/player.png");
    if (playerTexture == NULL) {
        SDL_LogError(1, "SDL player texture load error: %s\n", SDL_GetError());
        return SDL_INIT_ERROR_CODE;
    }

    int playerWidth, playerHeight;
    SDL_QueryTexture(playerTexture, NULL, NULL, &playerWidth, &playerHeight);

    SDL_Rect playerRect;
    playerRect.x = 10;
//    playerRect.x = WINDOW_WIDTH / 2;
    playerRect.y = WINDOW_HEIGHT / 2 - (playerHeight/ 2);

    playerRect.w = playerWidth;
    playerRect.h = playerHeight;

    SDL_Texture* ballTexture = IMG_LoadTexture(renderer, "./assets/ball.png");
    if (playerTexture == NULL) {
        SDL_LogError(1, "SDL ball texture load error: %s\n", SDL_GetError());
        return SDL_INIT_ERROR_CODE;
    }

    SDL_Texture* enemyTexture = IMG_LoadTexture(renderer, "./assets/player.png");
    if (enemyTexture == NULL) {
        SDL_LogError(1, "SDL enemy texture load error: %s\n", SDL_GetError());
        return SDL_INIT_ERROR_CODE;
    }

    int enemyWidth, enemyHeight;
    SDL_QueryTexture(enemyTexture, NULL, NULL, &enemyWidth, &enemyHeight);

    SDL_Rect enemyRect;
    enemyRect.x = WINDOW_WIDTH - enemyWidth - 10;
    enemyRect.y = WINDOW_HEIGHT / 2 - (enemyHeight/ 2);

    enemyRect.w = enemyWidth;
    enemyRect.h = enemyHeight;

    int ballWidth, ballHeight;
    SDL_QueryTexture(ballTexture, NULL, NULL, &ballWidth, &ballHeight);

    SDL_Rect ballRect;
    ballRect.x = WINDOW_WIDTH / 2 - (ballWidth / 2);
    ballRect.y = WINDOW_HEIGHT / 2 - (ballHeight/ 2);

    ballRect.w = ballWidth;
    ballRect.h = ballHeight;

    SDL_Event event;
    bool quit = false;

    const int BALL_SPEED = 10;

    short xDirection = -1;
    short yDirection = -1;

    while (!quit) {
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                quit = true;
                break;
            }
        }

        int cursorY;
        SDL_GetMouseState(NULL, &cursorY);

        playerRect.y = cursorY - (playerHeight/ 2);
        enemyRect.y = ballRect.y;

        if (ballRect.x <= 0 || ballRect.x >= WINDOW_WIDTH - ballRect.w) {
            xDirection *= -1;
        }
        if (ballRect.y <= 0 || ballRect.y >= WINDOW_HEIGHT - ballRect.h) {
            yDirection *= -1;
        }

        if (((ballRect.x >= playerRect.x + playerRect.w - 1 && ballRect.x <= playerRect.x + playerRect.w) || (ballRect.x + ballRect.w <= playerRect.x + 1 && ballRect.x + ballRect.w >= playerRect.x)) && ((ballRect.y >= playerRect.y && ballRect.y <= playerRect.y + playerRect.h) || (ballRect.y + ballRect.h >= playerRect.y && ballRect.y + ballRect.h <= playerRect.y + playerRect.h))) {
            xDirection *= -1;
        }
        if (((ballRect.y >= playerRect.y + playerRect.h - 1 && ballRect.y <= playerRect.y + playerRect.h) || (ballRect.y + ballRect.h >= playerRect.y - 1 && ballRect.y + ballRect.h <= playerRect.y)) && ((ballRect.x >= playerRect.x && ballRect.x <= playerRect.x + playerRect.w) || (ballRect.x + ballRect.w >= playerRect.x && ballRect.x + ballRect.w <= playerRect.x + playerRect.w))) {
            yDirection *= -1;
        }

        ballRect.x = ballRect.x + BALL_SPEED * xDirection;
        ballRect.y = ballRect.y + BALL_SPEED * yDirection;

//        SDL_Log("xBall = %d; yBall = %d; xPlayer = %d; yPlayer = %d\n", ballRect.x, ballRect.y, playerRect.x, playerRect.y);
//        SDL_Log("x = %d; y = %d; xdir = %d; ydir = %d \n", ballRect.x, ballRect.y, xDirection, yDirection);

        SDL_SetRenderDrawColor(renderer, 200, 200, 200, 1);
        SDL_RenderClear(renderer);
        SDL_RenderCopy(renderer, playerTexture, NULL, &playerRect);
        SDL_RenderCopy(renderer, enemyTexture, NULL, &enemyRect);
        SDL_RenderCopy(renderer, ballTexture, NULL, &ballRect);
        SDL_RenderPresent(renderer);

        SDL_Delay(30);
    }

    SDL_DestroyTexture(playerTexture);
    SDL_DestroyTexture(ballTexture);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);

    SDL_Quit();
    return 0;
}
